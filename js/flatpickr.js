/**
 * @file
 * JavaScript integration file for the flatpickr module.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.flatpickr = {
    attach: function (context, settings) {

      if (typeof $().flatpickr === 'undefined') {
        return;
      }

      var settingsExists = settings.hasOwnProperty('flatpickr') && settings.flatpickr.hasOwnProperty('instances');

      $('.flatpickr', context).once('flatpickr', function () {
        var that = this;
        var id = $(this).attr('id');
        var iSettingsExists = settingsExists && settings.flatpickr.instances.hasOwnProperty(id);
        var options = (iSettingsExists && settings.flatpickr.instances[id].hasOwnProperty('options') ?
          settings.flatpickr.instances[id].options : {}
        );

        // Add in the confirmDate plugin.
        // @todo make configurable.
        if (options.hasOwnProperty('plugins')) {
          if (options.plugins.hasOwnProperty('confirmDate')) {
            options.plugins = [new confirmDatePlugin({})];
          }
        }

        // Allow the flatpickr to be appended to a specific DOM element.
        if (options.hasOwnProperty('appendToClosest')) {
          options.appendTo = $(this).closest(options.appendToClosest)[0];
          delete options.appendToClosest;
        }

        $(this)
          .attr('autocomplete', 'off')
          .flatpickr(options);
      });

    }
  };

})(jQuery, Drupal, this, this.document);
